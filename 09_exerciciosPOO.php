<?php 

abstract class Conta 
{
    public $tipoDeConta;
   public function getTipoDaConta()
   {
      return $this -> tipoDeConta;
   }
   public function setTipoDaConta(string $tipoDeConta)
   {
      $this -> tipoDeConta = $tipoDeConta;
   }
    public $agencia;
    public $conta;
    protected $saldo;

    public function imprimeExtrato()
    {
        echo 'Conta: '. $this-> tipoDeConta. ' Agência: '. $this->agencia . ' Conta: '.$this->conta. ' Saldo: '.$this->saldo;
        
    }

    public function deposito(float $valor)
    {
      if ($this->saldo>0)
      {
      $this ->saldo += $valor;
       echo "deposito realizado com sucesso <br>";
      } else{
        echo "Deposito Invalido!!<br>"; 

      }
    }

    public function saque(float $valor)
    {
        //$this->saldo = $this->saldo - $valor;
        if($this->saldo>=$valor)
        {
           $this-> saldo -=$valor;
            echo "Saque realizado com sucesso <br>";
        }else
        {
           echo"Saldo insuficiente!!!<br>";
        }
    }

    
    abstract public function calculaSaldo();
};

class Poupanca extends Conta
{
    public $reajuste;

    public function __construct(string $agencia,$conta,float $saldo)
    {
        $this->tipoDeConta = 'Poupanca';
        $this->conta = $conta;
        $this->agencia = $agencia;
        $this->saldo = $saldo;
    }

    public function calculaSaldo()
    {
         return $this->saldo + ($this->saldo * $this->reajuste/100);
    }
};

class Especial extends Conta
{
    public $saldoEspecial;

    public function __construct(string $agencia,$conta,float $saldoEspecial)
    {
        $this->tipoDeConta = 'Especial';
        $this->agencia = $agencia;
        $this->conta = $conta;
        $this->saldoEspecial = $saldoEspecial;
    }

    public function calculaSaldo()
    {
        return  $this->saldo + $this->saldoEspecial;
    }
};


$ctaPoup = new Poupanca ('0002-7', '85588-88',0.54);
$ctaPoup-> deposito(1500);
$ctaPoup-> saque(300);
$ctaPoup-> imprimeExtrato();

echo '<br>';

$ctaPoup = new Poupanca ('0055-2', '75588-42',2300);
//$ctaPoup-> saldo = -1500;
$ctaPoup-> deposito(1500);
$ctaPoup-> saque(300);
$ctaPoup-> imprimeExtrato();

?>