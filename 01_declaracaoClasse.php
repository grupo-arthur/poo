<?php

abstract class Forma
{
 public $tipoDeForma = 'Forma Abstrata';
 
 public function imprimeForma()
 
 {
	echo $this -> tipoDeForma;
}
	abstract public function calculaArea();
}
class Quadrados extends Forma 
{
	public $lado;
	  public function calculaArea()
	  {
		  return $this -> lado * $this -> lado;
	  }
}
 $obj = new quadrados();
 $obj -> tipoDeForma = 'Quadrados';
 $obj -> calculaArea();
 $obj ->  imprimeForma();
 
?>